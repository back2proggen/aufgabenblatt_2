package aufgabenblatt2.streams;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class StringVerarbeitung {

	public String leerzeichenAbschneiden(String wort) {

		int i = 0;
		while (wort.charAt(i) == ' ') {
			i++;
		}

		char[] wortArray = new char[wort.length() - i];

		for (int j = 0; j < wort.length() - i; j++) {
			wortArray[j] = wort.charAt(j + i);
		}

		wort = new String(wortArray);

		i = 0;
		while (wort.charAt(wort.length() - 1 - i) == ' ') {
			i++;
		}

		wortArray = new char[wort.length() - i];

		for (int j = 0; j < wortArray.length; j++) {
			wortArray[j] = wort.charAt(j);
		}

		return new String(wortArray);
	}

	public String capitalLetters(String wort) {
		char[] wot = wort.toCharArray();

		for (int i = 0; i < wot.length; i++) {
			if (wot[i] > 96) {
				wot[i] = (char) (wot[i] - 32);
			}
		}

		return String.valueOf(wot);
	}

	public String ersetzen(String wort) {
		List<Character> wortList = new ArrayList<Character>();

		for (int i = 0; i < wort.length(); i++) {
			if (wort.charAt(i) > 126) {
				switch (wort.charAt(i)) {
				case 228:
					// �
				case 196:
					// �
					wortList.add('A');
					wortList.add('E');
					break;
				case 246:
					// �
				case 214:
					// �
					wortList.add('O');
					wortList.add('E');
					break;
				case 252:
					// �
				case 220:
					// �
					wortList.add('U');
					wortList.add('E');
					break;
				case 223:
					// �
					wortList.add('S');
					wortList.add('S');
					break;
				}
			} else {
				wortList.add(wort.charAt(i));
			}
		}

		char[] wortArray = new char[wortList.size()];
		for (int i = 0; i < wortList.size(); i++)
			wortArray[i] = wortList.get(i);
		return new String(wortArray);
	}

	public String kuerzen(String wort) {
		if (wort.length() > 8) {
			char[] wortArray = new char[8];
			for (int i = 0; i < wortArray.length; i++)
				wortArray[i] = wort.charAt(i);

			wort = new String(wortArray);
		}
		return wort;
	}
}
