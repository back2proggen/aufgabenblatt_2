package aufgabenblatt2.threads;

public class main {
	
	public static void main(String[] args) throws InterruptedException {
		
		Rennauto erste = new Rennauto(1, 10, "wagen 1");
		Rennauto zweite = new Rennauto(1, 10, "wagen 2");
		Rennauto dritte = new Rennauto(1, 10, "wagen 3");

		erste.start();
		zweite.start();
		dritte.start();
			
		erste.join();
		zweite.join();
		dritte.join();

		System.out.println("Rennen zuende");
		System.out.println("Ergebnis:");
		Rennauto.eintrage.forEach(s-> System.out.println(s.toString()));
			

		

	}

}
