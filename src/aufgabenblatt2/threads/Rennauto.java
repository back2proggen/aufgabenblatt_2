package aufgabenblatt2.threads;

import java.util.ArrayList;
import java.util.List;

public class Rennauto extends Thread {

	private double geschwindigkeit;
	private double Streckenlšnge;
	private double aktuellePosition;
	private double sekundenAnzahl;
	private String threadName;
	private double diff;
	public static List<Rennauto>eintrage;

	@Override
	public String toString() {
		return ""+threadName+" "+ getDiff()+ "sek";
	}

	public Rennauto(int vs, int streckenlšnge, String name) {
		geschwindigkeit = vs;
		Streckenlšnge = streckenlšnge;
		threadName = name;
		eintrage = new ArrayList<>();
	}

	@Override
	public void run() {
		long anfang = System.currentTimeMillis();

		Thread.currentThread().setName(threadName);

		while (aktuellePosition < Streckenlšnge) {

			try {
				Thread.sleep((long) ((Math.rint(10 * ((Math.random() * 0.4) + 0.8)) / 10) * 1000));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			sekundenAnzahl = 1 + sekundenAnzahl;
			aktuellePosition = Math.ceil((geschwindigkeit * sekundenAnzahl));

			System.out.println(Thread.currentThread().getName() + ": " + aktuellePosition + "/" + Streckenlšnge);

		}

		long ende = System.currentTimeMillis();
		diff = (ende - anfang);
		eintrage.add(this);

	}

	public double getDiff() {
		return Math.rint(10*(diff/1000))/10;
	}

}
