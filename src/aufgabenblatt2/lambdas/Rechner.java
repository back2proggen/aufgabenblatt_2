package aufgabenblatt2.lambdas;

import java.util.HashMap;
import java.util.Map;

public class Rechner {
	
	public enum rechenart{ADDITION,SUBTRAKTION,MULIPLIKATION,DIVISION};
	private Map<rechenart,BinaryOperator> eintraege = new HashMap<>();
	
	private BinaryOperator add = (zahl1,zahl2)-> zahl1+zahl2;
	private BinaryOperator sub = (zahl1,zahl2)-> zahl1-zahl2;
	private BinaryOperator mul = (zahl1,zahl2)-> zahl1*zahl2;
	private BinaryOperator div = (zahl1,zahl2)-> zahl1/zahl2;

	public Rechner() {
		
		eintraege.put(rechenart.ADDITION, add);
		eintraege.put(rechenart.SUBTRAKTION, sub);
		eintraege.put(rechenart.MULIPLIKATION,mul);
		eintraege.put(rechenart.DIVISION, div);
	}

	public double berechne(rechenart operation,double wert , double wert2)
	{
		return eintraege.get(operation).verrechne(wert, wert2);
	}

}
