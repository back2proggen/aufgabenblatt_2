package aufgabenblatt2.lambdas;

public interface BinaryOperator {

	public double verrechne(double zahl1 ,double zahl2);
}
